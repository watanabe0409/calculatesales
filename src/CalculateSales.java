
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
public class CalculateSales {
	public static void main(String[] args){
		Map<String, String> map = new HashMap<String, String>();
		System.out.println("ここにあるファイルを開きます");
		BufferedReader br = null;
		try{
			File file = new File (args[0], "branch.lst");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);



			String line;
			while((line = br.readLine()) != null) {
				System.out.println(line);
				String items[] = line.split(",");
				map.put(items[0], items[1]);
			}
System.out.print(map);
		} catch(IOException e) {
			System.out.println("エラーが発生しました");
		} finally {
			if(br != null){
				try{
					br.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした");
				}


			}
		}
	}
}
