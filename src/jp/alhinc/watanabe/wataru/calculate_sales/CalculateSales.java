package jp.alhinc.watanabe.wataru.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class CalculateSales {
	public static void main(String[] args){
		Map<String, String> salesmap = new HashMap<String, String>();
		Map<String, String> amazingmap = new HashMap<String, String>();
		Map<String, Long> mannymap = new HashMap<String, Long>();
		Map<String, Long> awesomemap = new HashMap<String, Long>();
		BufferedReader br = null;
		try{
			File file = new File (args[0], "branch.lst");
			if(!file.exists()){
				System.out.println("ファイルは存在しません。");
				return;
			}

			if(args.length == 0){
				System.out.println("予期せぬエラーが発生しました");
				return;
			}

			if(args.length >= 2){
				System.out.println("予期せぬエラーが発生しました");
				return;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);


			String line;
			while((line = br.readLine()) != null) {
				String items[] = line.split(",");
				if(!items[0].matches("\\d{3}")){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				salesmap.put(items[0], items[1]);
				mannymap.put(items[0],0L);
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if(br != null){
				try{
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}


			}
		}

		BufferedReader ar = null;
		try{
			File file = new File (args[0], "commodity.lst");
			if(!file.exists()){
				System.out.println("ファイルは存在しません。");
				return;
			}

			FileReader zr = new FileReader(file);
			ar = new BufferedReader(zr);


			String line;
			while((line = ar.readLine()) != null) {
				String items[] = line.split(",");
				amazingmap.put(items[0], items[1]);
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if(ar != null){
				try{
					ar.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}


		BufferedReader xl = null;
		File dir = new File (args[0]);
		File[] list = dir.listFiles();
		List<String> lastList = new ArrayList<String>();
		for(int i = 0; i < list.length; i++){
			if(list[i].getName().toString().matches("\\d{8}.rcd")){
				lastList.add(list[i].getName().toString());
			}
		}
		int first = 0;
		int end = 8;
		for(int g = 1; g < list.length; g++){
			if(list[g].getName().toString().matches("\\d{8}.rcd")){
				if(!list[g].isFile()){
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
				int total = Integer.parseInt(list[g-1].getName().substring(first, end).toString());
				int fa = Integer.parseInt(list[g].getName().substring(first, end).toString());
				if(fa - total != 1){
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}
		}
		ArrayList<String> salesum = null;
		for(int j = 0; j < lastList.size(); j++){
			try{
				salesum = new ArrayList<String>();

				File salesFile = new File (args[0], lastList.get(j));
				FileReader fr = new FileReader(salesFile);
				xl= new BufferedReader(fr);
				String saleFile;
				while((saleFile = xl.readLine()) != null) {
					salesum.add(saleFile);
					if (!salesmap.containsKey(salesum.get(0))){
						System.out.println("<" + list[j].getName().toString() + ">" + "の支店コードが不正です");
						return;
					}
				}
				if(!salesum.get(2).matches("\\d{1,10}")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				if(salesum.size() >= 4){
					System.out.println(list[j].getName().toString() + "のフォーマットが不正です");
					return;
				}
				if(salesum.size() <= 1){
					System.out.println(list[j].getName().toString() + "のフォーマットが不正です");
					return;
				}
				salesum.get(0);
				long aftersales = mannymap.get(salesum.get(0)) + Long.parseLong(salesum.get(2));

				String.valueOf(aftersales);
				if(String.valueOf(aftersales).length() > 10){
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				mannymap.put(salesum.get(0),aftersales);
				awesomemap.put(salesum.get(1), aftersales);
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;

			} finally {
				if(xl != null){
					try{
						xl.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}



		}



		List<Map.Entry<String,Long>> entries =
				new ArrayList<Map.Entry<String,Long>>(mannymap.entrySet());
		Collections.sort(entries, new Comparator<Map.Entry<String,Long>>() {

			@Override
			public int compare(Entry<String,Long> entry1, Entry<String,Long> entry2) {
				return ((Long)entry2.getValue()).compareTo((Long)entry1.getValue());
			}
		});

		List<Map.Entry<String,Long>> send =
				new ArrayList<Map.Entry<String,Long>>(awesomemap.entrySet());
		Collections.sort(send, new Comparator<Map.Entry<String,Long>>() {

			@Override
			public int compare(Entry<String,Long> entry1, Entry<String,Long> entry2) {
				return ((Long)entry2.getValue()).compareTo((Long)entry1.getValue());
			}
		});

		BufferedWriter rw = null;
		try{
			File FinalFile = new File(args[0],"branch.out");
			FileWriter fw = new FileWriter(FinalFile);
					rw = new BufferedWriter(fw);
					for (Entry<String,Long> s : entries) {
						rw.write(s.getKey() + "," + salesmap.get(s.getKey()) + "," + s.getValue() + "\n");
					}

		}  catch(IOException e) {
			System.out.println(e);
		}
		finally {
			if(rw != null){
				try{
					rw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}


		BufferedWriter kw = null;
		try{
			File FinalFile = new File(args[0],"commodity.out");
			FileWriter lw = new FileWriter(FinalFile);
			kw = new BufferedWriter(lw);
			for (Entry<String,Long> s : send) {
				kw.write(s.getKey() + "," + amazingmap.get(s.getKey()) + "," + s.getValue() + "\n");
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		finally {
			if(kw != null){
				try{
					kw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
	}
}