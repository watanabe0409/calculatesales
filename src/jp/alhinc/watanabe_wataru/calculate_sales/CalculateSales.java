package jp.alhinc.watanabe_wataru.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class CalculateSales {
	public static void main(String[] args){
		Map<String, String> salesmap = new HashMap<String, String>();
		Map<String, String> amazingmap = new HashMap<String, String>();
		Map<String, Long> mannymap = new HashMap<String, Long>();
		Map<String, Long> awesomemap = new HashMap<String, Long>();

		if(args.length == 0){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		if(args.length >= 2){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		if(!readingFile(args[0], "branch.lst", salesmap, mannymap, "\\d{3}" ,"支店定義")){
			return;
		}


		if(!readingFile(args[0], "commodity.lst", amazingmap, awesomemap, "^[A-Za-z0-9]{8}$" ,"商品定義")){
			return;
		}

		BufferedReader xl = null;
		File dir = new File (args[0]);
		File[] list = dir.listFiles();
		List<String> lastList = new ArrayList<String>();
		for(int u = 0; u < list.length -1; u++){

			if(list[u].getName().toString().matches("\\d{8}.rcd") && list[u].isFile()){
				lastList.add(list[u].getName().toString());
			}
		}
		for(int u = 0; u < lastList.size() -1; u++){

			String fileName = lastList.get(u);
			int fileNumber = Integer.parseInt(fileName.substring(0,8));
			String fileNames = lastList.get(u + 1);
			int fileNumbers = Integer.parseInt(fileNames.substring(0,8));
			if(fileNumbers - fileNumber != 1){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}

		}
		ArrayList<String> salesum = null;
		for(int j = 0; j < lastList.size(); j++){
			try{
				salesum = new ArrayList<String>();

				File salesFile = new File (args[0], lastList.get(j));
				FileReader fr = new FileReader(salesFile);
				xl= new BufferedReader(fr);
				String saleFile;
				while((saleFile = xl.readLine()) != null) {
					salesum.add(saleFile);
					}

				if(salesum.size() != 3){
					System.out.println(list[j].getName().toString() + "のフォーマットが不正です");
					return;
				}
				if (!salesmap.containsKey(salesum.get(0))){
					System.out.println(list[j].getName().toString() + "の支店コードが不正です");
					return;
				}
				if (!amazingmap.containsKey(salesum.get(1))){
					System.out.println(list[j].getName().toString() + "の商品コードが不正です");
					return;
				}
				if(!salesum.get(2).matches("\\d+")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				long aftersales = mannymap.get(salesum.get(0)) + Long.parseLong(salesum.get(2));
				long salessums = awesomemap.get(salesum.get(1)) + Long.parseLong(salesum.get(2));

				String.valueOf(aftersales);
				if(String.valueOf(aftersales).length() > 10){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				String.valueOf(salessums);
				if(String.valueOf(salessums).length() > 10){
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				mannymap.put(salesum.get(0),aftersales);
				awesomemap.put(salesum.get(1), salessums);
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;

			} finally {
				if(xl != null){
					try{
						xl.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}



		}

		if(!writingFile(args[0], "branch.out", mannymap, salesmap)){
			return;
		}
		if(!writingFile(args[0], "commodity.out", awesomemap, amazingmap)){
			return;
		}

	}
	public static boolean readingFile (String path, String input, Map<String, String> firstsmap, Map<String, Long> secondmap, String number, String name){
		BufferedReader br = null;
		try{
			File file = new File (path, input);
			if(!file.exists()){
				System.out.println(name +"ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);


			String line;
			while((line = br.readLine()) != null) {
				String items[] = line.split(",");
				if(!items[0].matches(number)){
					System.out.println(name + "ファイルのフォーマットが不正です");
					return false;
				}
				if(items.length != 2){
					System.out.println(name +"ファイルのフォーマットが不正です");
					return false;
				}
				firstsmap.put(items[0], items[1]);
				secondmap.put(items[0],0L);
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		return false;
		} finally {
			if(br != null){
				try{
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
	public static boolean writingFile(String path, String output, Map<String, Long> totalmap, Map<String, String> salesmap){


		List<Map.Entry<String,Long>> entries =
				new ArrayList<Map.Entry<String,Long>>(totalmap.entrySet());
		Collections.sort(entries, new Comparator<Map.Entry<String,Long>>() {

			@Override
			public int compare(Entry<String,Long> entry1, Entry<String,Long> entry2) {
				return ((Long)entry2.getValue()).compareTo((Long)entry1.getValue());
			}
		});

		BufferedWriter kw = null;
		try{
			File FinalFile = new File(path , output);
			FileWriter lw = new FileWriter(FinalFile);
			kw = new BufferedWriter(lw);
			for (Entry<String,Long> s : entries) {

				kw.write(s.getKey() + "," + salesmap.get(s.getKey()) + "," + s.getValue());
				kw.newLine();
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		finally {
			if(kw != null){
				try{
					kw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}

